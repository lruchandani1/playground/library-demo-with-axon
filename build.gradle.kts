plugins {
	java
//	val kotlinVersion = "1.2.71"
//	kotlin("jvm") version kotlinVersion
//	kotlin("plugin.spring") version kotlinVersion
	id("org.springframework.boot") version "2.2.4.RELEASE"
	id("io.spring.dependency-management") version "1.0.9.RELEASE"
}

group = "com.lalit.library"
version = "0.0.1-SNAPSHOT"

java {
	sourceCompatibility = JavaVersion.VERSION_11
	targetCompatibility = JavaVersion.VERSION_11
}
repositories {
	mavenCentral()
	maven(url="https://repo.spring.io/milestone")
}
dependencies {
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation( "org.springframework.boot:spring-boot-starter-webflux")
	runtimeOnly("mysql:mysql-connector-java")
	implementation( "org.axonframework:axon-spring-boot-starter:4.3"){
		exclude(group="org.axonframework",module = "axon-server-connector")
	}
	testImplementation("org.springframework.boot:spring-boot-starter-test") {
		exclude(group="org.junit.vintage",module ="junit-vintage-engine")
	}
	testImplementation("io.projectreactor:reactor-test")
	compileOnly("org.projectlombok:lombok:1.18.12")
	annotationProcessor("org.projectlombok:lombok:1.18.12")
}

