package com.lalit.library;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LibraryWithAxonApplication {

	public static void main(String[] args) {
		SpringApplication.run(LibraryWithAxonApplication.class, args);
	}

}
