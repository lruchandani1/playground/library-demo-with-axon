package com.lalit.library.command;

import lombok.Data;

import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
public class AddBookCommand {
@TargetAggregateIdentifier
private String isbn;
private String name;
private String author;
}
