package com.lalit.library.command;

import lombok.Data;

import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
public class IssueBookCommand {
  @TargetAggregateIdentifier
  private String isbn;
}
