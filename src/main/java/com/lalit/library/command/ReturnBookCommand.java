package com.lalit.library.command;

import lombok.Data;

import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
public class ReturnBookCommand {
  @TargetAggregateIdentifier
  private String isbn;
}
