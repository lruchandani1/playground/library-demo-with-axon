package com.lalit.library.controller;

import java.util.List;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

import com.lalit.library.command.AddBookCommand;
import com.lalit.library.model.Book;

@RestController
public class BookController {

  @Autowired
  CommandGateway commandGateway;

  @Autowired
  QueryGateway queryGateway;

  @PostMapping(value = "/books")
  public Mono<String> addBook(@RequestBody AddBookCommand addBookCommand){
    return Mono.create(sink ->commandGateway.send(addBookCommand)
                      .thenAccept(val -> sink.success(val.toString()))
                      .exceptionally(th -> {
                        sink.error(th);
                        return null;
                      }));
  }


  @GetMapping(value = "/books/{bookId}")
  public Mono<Book> getOne(@PathVariable("bookId") String bookId){
    return Mono.create( sink -> queryGateway.query("findOne",bookId,Book.class)
                                .thenAccept(result -> sink.success(result))
                                .exceptionally(th -> {
                                  sink.error(th);
                                  return null;
                                }));
  }

  @GetMapping(value = "/books")
  public Mono<List<Book>> getAll(){
    return Mono.create( sink -> queryGateway.query("findAll",null, ResponseTypes.multipleInstancesOf(Book.class))
        .thenAccept(result -> sink.success(result))
        .exceptionally(th -> {
          sink.error(th);
          return null;
        }));
  }

}
