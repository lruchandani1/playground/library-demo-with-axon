package com.lalit.library.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;

import com.lalit.library.command.AddBookCommand;
import com.lalit.library.command.IssueBookCommand;
import com.lalit.library.command.ReturnBookCommand;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Data
@Entity
@Aggregate
public class Book {

  @AggregateIdentifier
  @Id
  private String bookId;

  private String name;
  private String author;
  private boolean rented;

  public Book() {
  }

  @CommandHandler
  public Book(AddBookCommand addBookCommand){
    log.info("Add Book Command : {}");
    this.bookId = addBookCommand.getIsbn();
    this.name = addBookCommand.getName();
    this.author = addBookCommand.getAuthor();
    this.rented = false;
  }

@CommandHandler
  public void handle(IssueBookCommand issueBookCommand){
    log.info("Issue Book : {}",issueBookCommand.getIsbn());
    if(this.isRented()){
      throw new IllegalStateException("Book Already rented");
    }
    this.rented=true;
  }

  @CommandHandler
  public void handle(ReturnBookCommand returnBookCommand){
    log.info("return book :{}",returnBookCommand.getIsbn());
    this.rented = false;
  }
}
