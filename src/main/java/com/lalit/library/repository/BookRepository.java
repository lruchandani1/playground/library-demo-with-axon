package com.lalit.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lalit.library.model.Book;


@Repository("bookQueryRepository")
public interface BookRepository extends JpaRepository<Book, String> {

}