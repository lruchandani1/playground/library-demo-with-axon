package com.lalit.library.service;

import java.util.List;

import org.axonframework.queryhandling.QueryHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lalit.library.model.Book;
import com.lalit.library.repository.BookRepository;

@Service
public class BookQueryService {

  @Autowired
  BookRepository bookRepository;

  @QueryHandler(queryName = "findOne")
  public Book findOne(String bookId){
    return bookRepository.findById(bookId).orElseThrow();
  }

  @QueryHandler(queryName = "findAll")
  public List<Book> findAll(){
    return bookRepository.findAll();
  }
}
